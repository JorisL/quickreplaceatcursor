# QuickReplaceAtCursor
Sublime text plugin for quickly replacing one or more predefined characters at the cursor position with another set of characters.
This can for example be used to quickly insert unicode characters.

## Installation
**Via Package Control**  
Search for `QuickReplaceAtCursor`

**Manual**
Clone this project in your Packages folder under the name `QuickReplaceAtCursor`.  
`git clone https://JorisL@bitbucket.org/JorisL/quickreplaceatcursor.git QuickReplaceAtCursor`

## Usage
By default this plugin is configured to quickly replace a single character with it's corresponding greek alphabetical symbol, or replace several characters with a mathematical symbol.

This plugin is - for example - configure to replace the character "p" just before the cursor with the character "π", or "inf" with "∞" when executing the QuickReplaceAtCursor command (default ctrl+shift+g, or via the command pallate).

## Configuration
The settings of this plugin (`Preferences → Package Settings → QuickReplaceAtCursor → Settings - Default`) has one configuration parameter `replacements` which is a dictionary (list) with each set of characters to possibly be replaced followed by the corresponding characters to be replaced by.
This can be modified in the `Settings - User` file.
An example configuration can be:

```
{
    "replacements": {
        "a": "ɑ", // alpha
        "deg": "°",
    }
}
```

With this example configuration; when the text just before the cursor is "deg" (without quotes), this will be replaced by "°" (also without characters).
When the character before the cursor is "a", this will be replaced by "ɑ".
If neither is the case then nothing will happen.
