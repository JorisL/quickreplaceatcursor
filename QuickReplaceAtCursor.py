import sublime
import sublime_plugin


class QuickReplaceAtCursorCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        settings = sublime.load_settings('QuickReplaceAtCursor.sublime-settings')

        for selection in self.view.sel():
            # Get the length of the largest defined replacement key. This limits the selection that has to be stored
            # and checked for a possible match.
            max_key_length = len((sorted(settings.get('replacements').keys(), key=len, reverse=True)[0]))
            # Get the text before the cursor, with max_key_length as maximum string length
            cursor_pos = selection.begin()
            text_upto_cursor = self.view.substr(sublime.Region(max(cursor_pos - max_key_length, 0), cursor_pos))

            # Loop over each possible key in the user list of possible replacements, from large to small
            # By looping from large to small, it is possible to have defined replacements that are also a subset of
            # another defined replacement, for example tm->™ in combination with t->τ
            for key in sorted(settings.get('replacements').keys(), key=len, reverse=True):
                # If the current key matches the selection, replace it with the replacement value for that key
                if text_upto_cursor.endswith(key):
                    key_region = sublime.Region(cursor_pos - len(key), cursor_pos)
                    self.view.replace(edit, key_region, settings.get('replacements')[key])
                    break
